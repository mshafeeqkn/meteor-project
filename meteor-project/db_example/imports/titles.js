// We can avoid this folder by adding the following line in 
// main.js file in the root directory of this application.
// Titles = new Mongo.Collection('titles');

import {Mongo} from 'meteor/mongo';

export const Titles = new Mongo.Collection('titles');