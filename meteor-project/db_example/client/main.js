import './main.html';
import {Titles} from '../imports/titles.js';

Template.body.helpers({
	titles: function () {
		return Titles.find();
	}	
});