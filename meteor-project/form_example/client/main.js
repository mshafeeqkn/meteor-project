import './main.html';

Template.body.helpers({
	titles: function () {
		if (Session.get('hideFinished')) {
			return Titles.find({checked: {$ne: true}});
		} else {
			return Titles.find();
		}
	},
	hideFinished: function () {
		return Session.get('hideFinished');
	}
});

Template.body.events({
	'submit .title-form': function (event) {
		var title = event.target.title.value;
		
		Titles.insert({
			title: title,
			createdAt: new Date()
		});
		
		event.target.title.value = "";
		return false;
	},
	'change .hide-finished': function (event) {
		Session.set('hideFinished', event.target.checked);	
	}
});

Template.title.events({
	'click .toggle-chk': function (event) {
		Titles.update(this._id, {$set: {checked: !this.checked}});
	},
	'click .delete': function (event) {
		Titles.remove(this._id);
	}
});
